
# spi related functions 
# Emanuel Dutra, emanuel.dutra@ipma.pt 2021


import numpy as np
import scipy.stats as ss 
import scipy.special as sps

ZeroMax = 1./3. # maximum frequency of zero to be accepted in the gamma fit 

def rolling_sum(a, n=None,axis=0) :
  """
  Compute rolling sum 
  Addapted from:
  https://stackoverflow.com/questions/28288252/fast-rolling-sum
  """
  ret = np.cumsum(a, axis=axis, dtype=float)
  if axis==1:
    ret[:, n:] = ret[:, n:] - ret[:, :-n]
    ret[:,0:n-1]=np.nan
    return ret
  elif axis==0:
    ret[n:,:] = ret[n:,:] - ret[:-n,:]
    ret[0:n-1,:]=np.nan
    return ret

def fspi_fit(D,zeromax,dbg=0):

  nt,ngp = D.shape
  coef = np.zeros((2,ngp))*np.nan
  q = np.zeros((ngp))*np.nan

  q = (nt - sum(D>0.)) / float(nt)
  pp = np.nonzero(q<=zeromax)
  coef[0,pp[0]],coef[1,pp[0]] = fitgamma(D[:,pp[0]])
  pp = np.nonzero(coef[0,:] > 1000 )
  if len(pp[0]>0):
      print("Error in spi.fspi_fit:",len(pp[0]),ngp)
  coef[0,pp[0]] = np.nan
  coef[1,pp[0]] = np.nan

  if dbg > 0:
    import matplotlib.pyplot as plt
    ip = dbg
    prob = q[ip]+(1.-q[ip])*ss.gamma.cdf(D[:,ip],coef[0,ip],scale=coef[1,ip])
    plt.plot(np.sort(D[:,ip]),np.arange(0,nt)/float(nt),'or')
    plt.plot(D[:,ip],prob,'.b')
    plt.show()

  return coef,q
  
def fspi_eval(D,zeromax,coef,q):
  #xspi = spi.fspi_eval(xprecTMP,coef,q)
  nt,ngp = D.shape
  xspi = np.zeros((nt,ngp))*np.nan


  for ip in range(ngp):
    if q[ip] > zeromax or np.isnan(coef[1,ip]) :
      continue
    xspi[:,ip] = q[ip]+(1.-q[ip])*ss.gamma.cdf(D[:,ip],coef[0,ip],scale=coef[1,ip])
  xspi[xspi < 0.001 ] = 0.001
  xspi[xspi > 0.999 ] = 0.999
  xspi[:,:] = ss.norm.ppf(xspi)

  xspi[np.isnan(D)]=np.nan
  return xspi
  
def fitgamma ( samples ): 
  """fit a gamma distribution using maximum likelihood 
   http://psignifit.sourceforge.net/api/pypsignifit.psigsimultaneous-pysrc.html#fitgamma
     Parameters 
     ---------- 
     samples : array 
         array of samples on which the distribution should be fitted 
  
     Returns 
    ------- 
    prm : sequence 
        pair of k (shape) and theta (scale) parameters for the fitted gamma distribution 
   
  """
  #np.seterr(invalid='raise')
  nt,ngp = samples.shape
  xmean = np.zeros((ngp))
  xmeanl = np.zeros((ngp))
  for ip in range(ngp):
    xx = samples[samples[:,ip]>0.,ip]
    xmean[ip] = np.mean(xx)
    xmeanl[ip] = np.mean( np.log(xx))

  s = np.log ( xmean ) - xmeanl
  k = 3 - s + np.sqrt ( (s-3)**2 + 24*s)
  k /= 12 * s
  
  ppbad = np.nonzero(s==0)[0]
  ppbad = np.concatenate((ppbad,np.nonzero(k<=0)[0]))
  k[ppbad]=7.8
  s[ppbad]=0.06

  for i in range ( 5 ):
    k -= ( np.log(k) - sps.digamma ( k ) -s ) / ( 1./k - sps.polygamma( 1, k ) )
    ppbad = np.concatenate((ppbad,np.nonzero(k<=0)[0]))
    k[ppbad]=7.8
  th = xmean / k 
  
  if len(ppbad)> 0:
    print(" spi.fitgamma: mle failed,",np.unique(ppbad))
  k[ppbad]=np.nan
  th[ppbad]=np.nan

  return k,th





