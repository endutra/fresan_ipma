## Exemplo para calcular e guardar SPI 
## Emanuel Dutra, emanuel.dutra@ipma.pt 2021 


import numpy as np 
from netCDF4 import Dataset, num2date 
import datetime as dt 
import matplotlib.pyplot as plt
import time 
import os 
import libspi as lspi

def calc_spi(xtp,xtime,dstartC,dendC,spi_tscale):
    
  t0 = time.time()
  ##==================================
  ## 1. Accumular a precipitaÃ§Ã£o
  xprec_acc = lspi.rolling_sum(xtp,n=spi_tscale,axis=0)
  xprec_acc[0:spi_tscale-1,:]=np.nan
  ntt,nlat,nlon = xtp.shape    
    
      
  ###====================================
  ### 2. Fit e calcular spi 

  ## inicipalizar array para o spi 
  xspi = np.zeros((ntt,nlat,nlon))*np.nan
  xmon = np.array([ t.month for t in xtime ])
  ##loop para cada mês do ano 
  for im in range(12):
          
    ## indices a processar, para cada mÃªs
    immFit = ( (xmon==im+1) &
               (xtime >= dstartC ) &
               (xtime <= dendC ) )
    
    # fit a função gamma 
    nmm = np.sum(immFit)
    coef,q = lspi.fspi_fit(xprec_acc[immFit,:,:].reshape(nmm,nlat*nlon),lspi.ZeroMax)

    # calculo do spi 
    immCalc = (xmon==im+1)
    nmm = np.sum(immCalc)
    xtmp = lspi.fspi_eval(xprec_acc[immCalc,:,:].reshape(nmm,nlat*nlon),
                              lspi.ZeroMax,coef,q)  
    xspi[immCalc] =  xtmp.reshape(nmm,nlat,nlon) 
  
 
  print("Processed SPI for grid: ",ntt,nlat,nlon,"in %3.2f seconds"%(time.time()-t0))
  return xspi 


## ==================================
## Dados precipitação 
fdata='AO-lt-tot-prec-20210901.nc'
fout="spi.nc"  # output 

##================================
## Definição do periodo de referência para cálculo do fit  
dstartC=dt.datetime(1981,1,1)
dendC=dt.datetime(2020,12,31)
spi_tscales=[3,6,9,12,24]
zmiss=-999

##================================
## 1. carregar dados precipitação 
ncIN = Dataset(fdata,'r')
lat = ncIN.variables['latitude'][:]
lon = ncIN.variables['longitude'][:]
tp = ncIN.variables['tp'][:]
xtmp = num2date(ncIN.variables['time'][:],ncIN.variables['time'].units,
                 only_use_python_datetimes=True,only_use_cftime_datetimes=False)
xtime = np.array([ dt.datetime(t.year,t.month,1) for t in xtmp]) 
ntt,nlat,nlon = tp.shape

##====================================
## 2. Criar ouput netcdf 
if ( os.path.isfile(fout)) : 
  os.remove(fout)
ncOUT = Dataset(fout,'w',format='NETCDF4')

## Create dimensions
for cdim in ['time','latitude','longitude']:
  dlen = len(ncIN.dimensions[cdim])
  ncOUT.createDimension(cdim,dlen)

#variables: dimensions 
for cvarI in ['time','latitude','longitude']:
  cvar=ncOUT.createVariable(cvarI,ncIN.variables[cvarI].dtype,
                            ncIN.variables[cvarI].dimensions)
  # copy attributes
  for att in ncIN.variables[cvarI].ncattrs():
    setattr(cvar,att,getattr(ncIN.variables[cvarI],att))
  # copy actual data 
  cvar[:] = ncIN.variables[cvarI][:]
    
for tscale in spi_tscales:
  cvar=ncOUT.createVariable("spi"+str(tscale),'f4',('time','latitude','longitude'),
                            zlib=True,complevel=1,fill_value=zmiss)
  

      
ncIN.close()


##=====================================
### 3 Calcular SPI para cada time-scale e escrever para o output 
for tscale in spi_tscales:
  xspi = calc_spi(tp[:,:,:],xtime,dstartC,dendC,tscale)
  
  ncOUT.variables["spi"+str(tscale)][:,:,:] = np.ma.filled(np.ma.fix_invalid(xspi),zmiss)
  
  
ncOUT.close()
