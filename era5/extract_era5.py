## Script to extrat hourly ERA5 data and compute daily statistics 

import cdsapi
import os
import datetime as dt
import time
import subprocess 

def extract_era5h_mon(year,mon):
  # function to extract hourly era5 data for a particular year,month saving to fout
  t0 =time.time()
  bar={'product_type': 'reanalysis',
     'format': 'netcdf',
      'variable': ['2m_temperature', 'total_precipitation'],
      'year': year,
      'month': mon,
      'day': [d for d in range(1,31+1)],
      'time':['00:00', '01:00', '02:00',
            '03:00', '04:00', '05:00',
            '06:00', '07:00', '08:00',
            '09:00', '10:00', '11:00',
            '12:00', '13:00', '14:00',
            '15:00', '16:00', '17:00',
            '18:00', '19:00', '20:00',
            '21:00', '22:00', '23:00',],
     'grid': [0.25,0.25],
     'area': [-3,10,-19,25]}

  c = cdsapi.Client()
  fout = f"{BASE_OUT}/era5_hourly_{ddate.strftime('%Y%m')}.nc"
  #print(bar)
  if not os.path.isfile(fout):
    print(f"Extracting ERA5 to {fout}")
    c.retrieve('reanalysis-era5-single-levels',bar,fout)
  else:
    print("Already extract, skipping")
  print(f"Extracted in {time.time()-t0:4.2f} seconds")  
  return fout   


##======================
# Base location to save ERA5 data 
BASE_OUT="/work/ERA5/"
dstart=dt.datetime(1980,1,1)
dend=dt.datetime(2021,9,1)

dstart=dt.datetime(2021,8,1)
dend=dt.datetime(2021,8,1)

##======================
## Loop on months
ddate=dstart
while ddate <= dend : 

  ##======================  
  #extract raw hourly data 
  fout = extract_era5h_mon(ddate.year,ddate.month)

  ##======================
  ## Computed daily statistics:

  ## Daily precipitation 
  fout_d_p=f"{BASE_OUT}/era5_daily_tp_{ddate.strftime('%Y%m')}.nc"
  out = os.system(f"cdo -f nc4 -z zip_6 -b F32 -mulc,24000 -daymean -selvar,tp {fout} {fout_d_p}")
  
  ## Daily tmin
  fout_d_tmin=f"{BASE_OUT}/era5_daily_tmin_{ddate.strftime('%Y%m')}.nc"
  out = os.system(f"cdo -f nc4 -z zip_6 -b F32 -daymin -selvar,t2m {fout} {fout_d_tmin}")

  ## Daily tmax
  fout_d_tmax=f"{BASE_OUT}/era5_daily_tmax_{ddate.strftime('%Y%m')}.nc"
  out = os.system(f"cdo -f nc4 -z zip_6 -b F32 -daymax -selvar,t2m {fout} {fout_d_tmax}")

  ## Daily tmean
  fout_d_tmean=f"{BASE_OUT}/era5_daily_tmean_{ddate.strftime('%Y%m')}.nc"
  out = os.system(f"cdo -f nc4 -z zip_6 -b F32 -daymean -selvar,t2m {fout} {fout_d_tmean}")
  
  ##======================
  ## Computed monthly statistics:
  
  ##  precip
  fout_m_p=f"{BASE_OUT}/era5_mon_tp_{ddate.strftime('%Y%m')}.nc"
  out = os.system(f"cdo -f nc4 -timmean {fout_d_p} {fout_m_p}")
  
  ##  tmin
  fout_m_tmin=f"{BASE_OUT}/era5_mon_tmin_{ddate.strftime('%Y%m')}.nc"
  out = os.system(f"cdo -f nc4 -timmean {fout_d_tmin} {fout_m_tmin}")

  ## Daily tmax
  fout_m_tmax=f"{BASE_OUT}/era5_mon_tmax_{ddate.strftime('%Y%m')}.nc"
  out = os.system(f"cdo -f nc4 -timmean {fout_d_tmax} {fout_m_tmax}")

  ## Daily tmean
  fout_m_tmean=f"{BASE_OUT}/era5_mon_tmean_{ddate.strftime('%Y%m')}.nc"
  out = os.system(f"cdo -f nc4 -timmean {fout_d_tmean} {fout_m_tmean}")

  ##======================
  ## Increment 1 month 
  ddate=(ddate+dt.timedelta(days=33)).replace(day=1)
  
