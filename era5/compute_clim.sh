#!/bin/bash 


# Script to compute montlhy means and other statistics 

set -eux 

DBASE=/work/ERA5

##===================
##  Compute monthly means 

# dstart=198001
# dend=202110
# 
# ddate=$dstart
# while [ $ddate -le $dend ]
# do
#   for cv in tp tmin tmax tmean
#   do
#     cdo timmean $DBASE/era5_daily_${cv}_${ddate}.nc $DBASE/era5_mon_${cv}_${ddate}.nc
#   done
#   ddate=$( date -u -d "${ddate}01 + 1 month" +"%Y%m")
# done 

## Compute tp normal climate of TP 1981 2010 

# # merge tp 
cdo mergetime $DBASE/era5_mon_tp_19????.nc $DBASE/era5_mon_tp_20????.nc era5_mon_tp_tmp.nc
cdo -ymonmean -muldpm -seldate,1981-01-01,2010-12-31 era5_mon_tp_tmp.nc era5_mon_tp_8110.nc
rm -f era5_mon_tp_tmp.nc

cdo -setday,1 -settime,00:00:00 -selmon,10 era5_mon_tp_8110.nc AO-no8110paah-tot-prec-20101001.nc 
fbb=AO-no8110paah-tot-prec-20101001.nc
for mm in 11 12 1 2 3 4 5 6 7 8 9
do
  m1=$(printf %02i $mm) 
  fout=AO-no8110paah-tot-prec-2010${m1}01.nc
  cdo -setday,1 -settime,00:00:00 -setmon,$mm -add $fbb -selmon,$mm era5_mon_tp_8110.nc $fout
  fbb=$fout
done 
rm -f era5_mon_tp_8110.nc era5_mon_tp_tmp.nc 
