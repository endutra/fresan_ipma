#!/bin/bash 

set -eux

export PATH="$PATH:/work/apps/utils/bin/"
/work/apps/py9/bin/python3 -u /home/ipma/fresan_ipma/forecasts/pp_forecasts.py $1 
