#!/bin/ksh 

# E. Dutra November 2021



set -exu



ymd=$1 #20160111 #$1
type=$2 #cf #pf  # cf
ODIR=$3
tt=0


mkdir -p $ODIR
mgrid='grid=0.25/0.25,area=-3/10/-19/25'

step=$( seq -s "/" 0 24 360)
step1=$( seq -s "/" 24 24 360)
step0=$( seq -s "/" 0 24 336)

if [[ $type = cf ]]; then 
  add=""
  FOUT=AO-dd-cf-prec-$ymd.nc
elif [[ $type = pf ]];then
  add="number=1/to/50,"
  FOUT=AO-dd-pf-prec-$ymd.nc
fi


WDIR=$TMPDIR/tmp/extract_precip_${ymd}${tt}${type}
mkdir -p $WDIR
cd $WDIR 
trap "rm -rf '$WDIR'" exit

rm -f *

## get raw data 
cat > bar << EOF
retrieve,
class=od,
date=$ymd,
expver=1,
levtype=sfc,
param=228.128,
step=$step,
stream=enfo,
time=$tt,
type=$type,
$mgrid,$add
target="output.grb"
EOF
mars < bar
# exit 0 

#deacc precip
cat > bar << EOF
read,source="output.grb",step=$step0,fieldset=stp0
read,source="output.grb",step=$step1,fieldset=stp1
compute,formula="max(0,stp1-stp0)*1000.",fieldset=out
write,fieldset=out,target="tmp.grb"
EOF
mars < bar


grib_to_netcdf -T -D NC_FLOAT tmp.grb -o $ODIR/$FOUT

rm -rfv $WDIR

exit 0 

