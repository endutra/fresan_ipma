E. Dutra November 2021
Details of the ensemble forecats data processed for Angola

get_forecast_data.ksh : handles a particular forecast data extraction
get_precip_ens.ksh : handles the ensemble daily precipitation forecast extraction 
get_tair_ens.ksh : handles the ensemble Tmin/Tmax forecast extraction 

The output files follow the convention: 
CC-DD-EEE-FFFF-yyyymmdd.nc
CC: region
        "AO" for Angola
DD:  Temporal integration
        "dd" for daily forecasts data
EEE: type of serie
        "cf"  : original control forecast
        "pf"  : original perfurbed forecasts : 50 members
        "avg" : ensemble mean 
        "std" : ensemble standard deviation
        "prc5/prc25/prc75/prc95" : ensemble percentile 5,25,75,95
FFFF: variable
        "prec" : precipitation
        "t2mn" : minimum temperature
        "t2mx" : maximum temperature 
      
e.g. file: AO-dd-avg-prec-20211101.nc  : Ensemble mean of forecast precipitation starting 
                                         on 20211101
** IMPORTANT: The time information in the netcdf files is at the end of the period. 
              For example the file AO-dd-avg-prec-20211101.nc has the time record: 
              2021-11-02 00:00:00  2021-11-03 00:00:00 ... 2021-11-16 00:00:00
              The first record is for the precipitaiton of day 2021-11-01. Same applies to the temperature. 
        
With all the data on disk(fdb) takes about 10min to process each forecast date on ecgate
