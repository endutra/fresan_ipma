#!/bin/ksh 

# E. Dutra February 2023

module load ecmwf-toolbox

set -exu



ymd=$1 #20160111 #$1
tt=$2  # time  0 or 12
type=$3 #cf #pf  # cf
ODIR=$4



mkdir -p $ODIR
mgrid='grid=0.15/0.15,area=42/-15/36/5'

step=$( seq -s "/" 0 1 48)
step1=$( seq -s "/" 1 1 48)
step0=$( seq -s "/" 0 1 47)

if [[ $type = cf ]]; then 
  add=""
  FOUT=AO-dd-cf-prec-$ymd${tt}.nc
elif [[ $type = pf ]];then
  add="number=1/to/50,"
  FOUT=AO-dd-pf-prec-$ymd${tt}.nc
fi


WDIR=$TMPDIR/tmp/extract_precip_${ymd}${tt}${type}
mkdir -p $WDIR
cd $WDIR 
trap "rm -rf '$WDIR'" exit

rm -f *

## get raw data 
cat > bar << EOF
retrieve,
class=od,
date=$ymd,
expver=1,
levtype=sfc,
param=228.128,
step=$step,
stream=enfo,
time=$tt,
type=$type,
$mgrid,$add
target="output.grb"
EOF
mars < bar
# exit 0 

#deacc precip
cat > bar << EOF
read,source="output.grb",step=$step0,fieldset=stp0
read,source="output.grb",step=$step1,fieldset=stp1
compute,formula="max(0,stp1-stp0)*1000.",fieldset=out
write,fieldset=out,target="tmp.grb"
EOF
mars < bar


grib_to_netcdf -T -D NC_FLOAT tmp.grb -o $ODIR/$FOUT

rm -rfv $WDIR

exit 0 

