from ecmwfapi import ECMWFService
import xarray as xr   
import numpy as np

fdate="20230610"
ftime="00"

  
# example to extract min/max temperature since last-post-processing 6-hourly
req={
    "class": "od",
    "date": fdate,
    "expver": "1",
    "levtype": "sfc",
    "param": "121.128/122.128",
    "step": "6/to/360/by/6",
    "stream": "enfo",
    "time": ftime,
    "type": "pf",
    "number":"1/to/50",
    "grid":"0.25/0.25",
    "area":"-3/10/-19/25",
    "format":"netcdf"
    }


# extract data   
server = ECMWFService("mars")
#server.execute(req,"target.nc")

# compute daily min/max 
ds = xr.open_dataset("target.nc")
ds_ = ds.copy(deep=True)
ds_.coords['time'] = ds_['time'] - np.timedelta64(3,'h') # shift time by -3 hours 
dsDay = ds_.resample(time='1D').max()    
dsDay['mn2t6'] = ds_['mx2t6'].resample(time='1D').min() 
# save to netcdf 
dsDay.encoding ={'latitude': {'zlib': False, '_FillValue': None},
                 'longitude': {'zlib': False, '_FillValue': None},
                 'mn2t6': {'_FillValue': -999.0,
                           'complevel': 1,
                           'zlib': True},
                 'mx2t6': {'_FillValue': -999.0,
                           'complevel': 1,
                           'zlib': True},
                  'time': {'dtype':'i4'}
                 }
dsDay.to_netcdf("target1.nc",encoding=dsDay.encoding)
