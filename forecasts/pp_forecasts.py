# script to pre-process ECMWF forecast 
#

import os 
import shutil
import glob 
import xarray as xr 
import numpy as np
import sys
import datetime as dt

def run_cmd(cmd):
    print("Running:",cmd)
    ret=os.system(cmd)
    if ret != 0:
        raise ValueError("Some problem running:",cmd)


ddate=sys.argv[1] #"2023061900"

DIRIN="/home/ipma"  # directory with RAW grib data 
DIROUTRAW="/home/ipma/old_forecasts/"  # directory to save RAW grib data 
DIROUT="/work/oper_forecasts/" # directory with final processed data 
DIRTMP="/dev/shm/"

expected_files=60
forecast_steps=range(6,360+6,6)

WDIR=os.path.join(DIRTMP,'pp_'+ddate)
if os.path.isdir(WDIR):
    shutil.rmtree(WDIR)
os.makedirs(WDIR,exist_ok=True)
print(dt.datetime.now()," Working on ",ddate,WDIR)

## RAW files structure
#ECMWF_ENFO_001_PF_SP_ANG_250_2023061900_{step} step 006 to 360  : 60 files   41.7Mb
# steps 6,12,18: mn2t6, mx2t6 102 fiels, 50 pf and 1 cf  609kb 
# steps 24 : mn2t6, mx2t6, tp , 153 fields 1012 kb 


##===================================================================================
## 1. check that all files are present 
raw_files = glob.glob(os.path.join(DIRIN,"ECMWF_ENFO_001_PF_SP_ANG_250_"+ddate+"*"))
if len(raw_files) != expected_files:
    print(f"Not all files are present. Found: {len(raw_files)}, expected: {expected_files}")
    sys.exit(-1)
    
##=====================================================================================
## For each step, split raw file as tmp_[step]_[shortName]_[dataType]
## example tmp_354_mn2t6_pf
for step in forecast_steps:
    finput=f"ECMWF_ENFO_001_PF_SP_ANG_250_{ddate}_{step:03d}"
    fout=f"tmp_{step:03d}_[shortName]_[dataType]"
    cmd=f"grib_copy {os.path.join(DIRIN,finput)} {os.path.join(WDIR,fout)}"
    print("Running:",cmd)
    os.system(cmd)
    
##=====================================================================================
## Concatenate into single files 
for ftype in ['cf','pf']:
    for cvar in ['mn2t6','mx2t6','tp']:
        finput=f"tmp_???_{cvar}_{ftype}"
        fout=f"tmp_{cvar}_{ftype}"
        foutNC=f"tmp_{cvar}_{ftype}.nc"
        # concatenate 
        cmd=f"cat {os.path.join(WDIR,finput)} > {os.path.join(WDIR,fout)}"
        print("Running:",cmd)
        os.system(cmd)
        # convert to netcdf 
        cmd=f"grib_to_netcdf -D NC_FLOAT -k 4 -d 6 -o {os.path.join(WDIR,foutNC)} {os.path.join(WDIR,fout)}"
        print("Running:",cmd)
        os.system(cmd)
        
        ## clean
        os.system(f"rm -f {os.path.join(WDIR,finput)}")
        os.system(f"rm -f {os.path.join(WDIR,fout)}")
        
##====================================================================================
## Daily post-processing:
##  find daily max / min temperatures
##  deaccumulate precipitation 
for ftype in ['cf','pf']:
    
    ## daily precipitation 
    finput=os.path.join(WDIR,f"tmp_tp_{ftype}.nc")
    foutput=os.path.join(DIROUT,f"AO-dd-{ftype}-prec-{ddate}.nc")
    print("Deaccumulate precipitation",foutput)
    ## process with xarry 
    ds = xr.open_dataset(finput)
    ## deaccumulate
    ds['tp'][1:,:] = ds['tp'][1:,:].values-ds['tp'][0:-1,].values
    ds['time'] = ds.time.values -np.timedelta64(12,'h') # shift time by 12 hour
    encoding = {'tp': {'zlib': True, 'complevel': 6}}#,'time':{'dtype':''}}
    ds.to_netcdf(foutput,encoding=encoding)
    ds.close()
    
    ## daily minimum temperatures
    finput=os.path.join(WDIR,f"tmp_mn2t6_{ftype}.nc")
    foutput=os.path.join(DIROUT,f"AO-dd-{ftype}-t2mn-{ddate}.nc")
    cmd=f"cdo -f nc4 -z zip_6 -daymin -shifttime,-3hour {finput} {foutput}"
    run_cmd(cmd)
    
    # daily maximum temperatures
    finput=os.path.join(WDIR,f"tmp_mx2t6_{ftype}.nc")
    foutput=os.path.join(DIROUT,f"AO-dd-{ftype}-t2mx-{ddate}.nc")
    cmd=f"cdo -f nc4 -z zip_6 -daymax -shifttime,-3hour {finput} {foutput}"
    run_cmd(cmd)
    
## All done, clean 
shutil.rmtree(WDIR)

## move RAW fils to final dir 
for ff in raw_files:
    os.rename(ff,os.path.join(DIROUTRAW,os.path.basename(ff)))
print(dt.datetime.now()," Finished ",ddate)
