import xarray as xr 
import pandas as pd 
import numpy as np 
import matplotlib.pyplot as plt 
import matplotlib as mplt
import matplotlib.dates as mdates
import datetime as dt 



BE5DATA='/work/ERA5/'
BFDATA='/work/forecast_data/'
daysBefore=29
ndayLead=15

stypes=['avg','prc5','prc25','prc75','prc95']
#stypes=['avg','std']

fcdate=dt.datetime.strptime('20221130','%Y%m%d')
dlist=pd.date_range(end=fcdate,freq='D',periods=daysBefore+1)
plat=-14.13
plon=14.91

## Now make the plot 
mplt.rc('xtick', labelsize=8) 
mplt.rc('ytick', labelsize=8)

fig = plt.figure(figsize=(6,9))

for ik,cvar in enumerate(['prec','t2mx','t2mn']):

    if cvar == 'prec':
        cvarNC='tp';cvarE5='tp';cvarE5NC='tp';yylabel="Precip (mm/day)";addoff=0
    elif cvar == 't2mx':
        cvarNC='mx2t6';cvarE5='tmax';cvarE5NC='t2m';yylabel="Tmax (C)";addoff=-273.16
    elif cvar == "t2mn":
        cvarNC='mn2t6';cvarE5='tmin';cvarE5NC='t2m';yylabel="Tmin (C)";addoff=-273.16



    # load all required data of ERA5 
    ffe=[]
    ffe.append(f"{BE5DATA}/era5_daily_{cvarE5}_{dlist[0].strftime('%Y%m')}.nc")
    if dlist[0].strftime('%Y%m') != dlist[-1].strftime('%Y%m') :
      ffe.append(f"{BE5DATA}/era5_daily_{cvarE5}_{dlist[-1].strftime('%Y%m')}.nc")

    xera = xr.open_mfdataset(ffe,concat_dim='time').sel(time=slice(dlist[0],dlist[-1]+dt.timedelta(days=1)))
    xeraT = [dt.datetime(x.year,x.month,x.day) for x in pd.DatetimeIndex(xera['time'].values)]

    # load all required files of the forecasts 
    xdata=[]
    for ddate in dlist:
      xx={}
      for tt in stypes:
        ff=f"{BFDATA}/AO-dd-{tt}-{cvar}-{ddate.strftime('%Y%m%d')}.nc"
        xx[tt]=xr.open_dataset(ff)
      xx['btime']=ddate
      xdata.append(xx)
      
    lat = xdata[0]['avg']['latitude'].values  
    lon = xdata[0]['avg']['longitude'].values

    ## select gridpoint to plot   
 
    ilat = np.argmin(np.abs(plat-lat))
    ilon = np.argmin(np.abs(plon-lon))

    ## select data for plots
    xplot={}
    for tt in stypes+['btime']:
      if tt == 'btime':
        xplot[tt] = np.zeros((ndayLead+daysBefore),'object')
      else:
        xplot[tt] = np.zeros((ndayLead+daysBefore),'f4')
        
    for tt in stypes+['btime']:
      for it,xx in enumerate(xdata):
        if tt == 'btime':
          xplot[tt][it]=pd.Timestamp(xx[tt])
        else:
          xplot[tt][it]=xx[tt][cvarNC].values[0,ilat,ilon]+addoff
      # add rest of forecast
      if tt == 'btime':
        xplot[tt][it+1:]=[pd.Timestamp(xa) for xa in xx['avg']['step'].values[0:-1]]
      else:
        xplot[tt][it+1:]=xx[tt][cvarNC].values[1:,ilat,ilon]+addoff
        
        

    ax = plt.subplot(3,1,ik+1)
    ax.fill_between(xplot['btime'][:],
                    xplot['prc95'][:],
                    xplot['prc5'][:],
                   ec='b',fc='b',alpha=0.2)
    ax.fill_between(xplot['btime'],xplot['prc75'],xplot['prc25'],
                   ec='b',fc='b',alpha=0.4)
                   
    #ax.fill_between(xplot['btime'][:],
                    #xplot['avg'][:]-xplot['std'][:],
                    #xplot['avg'][:]+xplot['std'][:],
                    #ec='b',fc='b',alpha=0.5)
                   
    ax.plot(xplot['btime'],xplot['avg'],lw=2,c='b')
    ax.plot(xeraT,xera[cvarE5NC][:,ilat,ilon]+addoff,lw=2,c='r')

    ax.plot([fcdate,fcdate],ax.get_ylim(),lw=2,c='k')
    ax.set_xlim([xplot['btime'][0],xplot['btime'][-1]])

    ax.set_xticks(xplot['btime'][0::2])
    ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d'))
    for label in ax.get_xticklabels(which='major'):
        label.set(rotation=30, horizontalalignment='right')
    ax.set_ylabel(yylabel,fontsize=8)
    ax.grid()
plt.tight_layout()
#plt.show()
ff=f"{cvar}_{fcdate.strftime('%Y%m%d')}_{lat[ilat]:4.2f}N_{lon[ilon]:4.2f}E.png"
fig.savefig(ff,dpi=300)
plt.close(fig)


    
