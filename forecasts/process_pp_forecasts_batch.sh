#!/bin/bash 

set -eux 

dstart=20230428
dend=20230621
dd=$dstart
while [ $dd -le $dend ]
do
    echo $dd
    time python3 -u pp_forecasts.py ${dd}00
    dd=$( date -u -d "$dd + 1 day" +%Y%m%d )
done 
