#!/bin/ksh 

#SBATCH -J fc_fresan
#SBATCH --qos=el
#SBATCH -o log.%x.%j.out
#SBATCH -e log.%x.%j.out
#SBATCH --mail-type=FAIL
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1

module load cdo
module load ecmwf-toolbox
module load ecaccess 

set -eux


## Assuming repository was cloned to $HOME/work/fresan_ipma
BASE=$HOME/work/fresan_ipma/forecasts

ODIR="$SCRATCH/tmp/fresan/"
mkdir -p $ODIR 


dstart=20221101
dend=20221205

ymd=$dstart

while [ $ymd -le $dend ];
do

  WDIR=$TMPDIR/tmp/date_${ymd}
  mkdir -p $WDIR
  cd $WDIR 
  trap "rm -rf '$WDIR'" exit


  ### PRECIP 
  $BASE/get_precip_ens.ksh $ymd cf $ODIR
  $BASE/get_precip_ens.ksh $ymd pf $ODIR 

  ### TAIR 
  $BASE/get_tair_ens.ksh $ymd cf $ODIR
  $BASE/get_tair_ens.ksh $ymd pf $ODIR 

  ## statistics 

  for cvar in t2mx t2mn prec
  do
    cd $WDIR
    cp $ODIR/AO-dd-cf-${cvar}-$ymd.nc pf000000.nc
    cdo splitlevel $ODIR/AO-dd-pf-${cvar}-$ymd.nc pf

    cdo -s -O -f nc4 -z zip_6 ensmean pf*.nc $ODIR/AO-dd-avg-${cvar}-$ymd.nc 
    for prct in 5 25 75 95
    do
      cdo -s -O -f nc4 -z zip_6 enspctl,$prct pf*.nc $ODIR/AO-dd-prc${prct}-${cvar}-$ymd.nc 
    done
    rm -f $WDIR/*
  done
  
  rm -rfv $WDIR
  
  cd $ODIR
  for ff in $(ls *-$ymd.nc)
  do
      ectrans -gateway ecgwx.ipma.pt -remote fresan_sftp -source $ff
  done 
  rm -f $ODIR/*-$ymd.nc

   
  ymd=$( date -u -d "${ymd} + 1 day" +%Y%m%d)
done
exit 0 
