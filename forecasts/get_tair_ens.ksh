#!/bin/ksh 

# E. Dutra November 2021



set -exu



ymd=$1 #20160111 #$1
type=$2 #cf #pf  # cf
ODIR=$3
tt=0


mkdir -p $ODIR
mgrid='grid=0.25/0.25,area=-3/10/-19/25'

step=$( seq -s "/" 6 6 360)
step6=$( seq -s "/" 6 24 360)
step12=$( seq -s "/" 12 24 360)
step18=$( seq -s "/" 18 24 360)
step24=$( seq -s "/" 24 24 360)

if [[ $type = cf ]]; then 
  add=""
  FOUTMAX=AO-dd-cf-t2mx-$ymd.nc
  FOUTMIN=AO-dd-cf-t2mn-$ymd.nc
elif [[ $type = pf ]];then
  add="number=1/to/50,"
  FOUTMAX=AO-dd-pf-t2mx-$ymd.nc
  FOUTMIN=AO-dd-pf-t2mn-$ymd.nc
fi


WDIR=$TMPDIR/tmp/extract_tair_${ymd}${tt}${type}
mkdir -p $WDIR
cd $WDIR 
trap "rm -rf '$WDIR'" exit

rm -f *

## get raw data 
cat > bar << EOF
retrieve,
class=od,
date=$ymd,
expver=1,
levtype=sfc,
param=121.128/122.128,
step=$step,
stream=enfo,
time=$tt,
type=$type,
$mgrid,$add
target="output.grb"
EOF
mars < bar
# exit 0 

#deacc precip
cat > bar << EOF
read,source="output.grb",step=$step6,param=mx2t6,fieldset=x6
read,source="output.grb",step=$step12,param=mx2t6,fieldset=x12
read,source="output.grb",step=$step18,param=mx2t6,fieldset=x18
read,source="output.grb",step=$step24,param=mx2t6,fieldset=x24
read,source="output.grb",step=$step6,param=mn2t6,fieldset=n6
read,source="output.grb",step=$step12,param=mn2t6,fieldset=n12
read,source="output.grb",step=$step18,param=mn2t6,fieldset=n18
read,source="output.grb",step=$step24,param=mn2t6,fieldset=n24
compute,formula="max(x24,x6,x12,x18)",fieldset=tx
compute,formula="min(n24,n6,n12,n18)",fieldset=tn
write,fieldset=tx,target="tmp_tx.grb"
write,fieldset=tn,target="tmp_tn.grb"
EOF
mars < bar


grib_to_netcdf -T -D NC_FLOAT tmp_tx.grb -o $ODIR/$FOUTMAX
grib_to_netcdf -T -D NC_FLOAT tmp_tn.grb -o $ODIR/$FOUTMIN

rm -rfv $WDIR

exit 0 

