import xarray as xr 
import pandas as pd 
import numpy as np 
import matplotlib.pyplot as plt 
import matplotlib as mplt
import matplotlib.dates as mdates
import datetime as dt 
import os



fcdate=dt.datetime.strptime('20230618','%Y%m%d')
BFDATA='/work/oper_forecasts/'
daysBefore=31
ndayLead=15
nmembers=50+1
dlist=pd.date_range(end=fcdate,freq='D',periods=daysBefore+1)
dtime=pd.date_range(start=dlist[0],freq='D',periods=daysBefore+ndayLead)
plat=-14.13
plon=14.91

vnames={'t2mx':'mx2t6',
        't2mn':'mn2t6',
        'prec':'tp'}

cvars=['t2mx','t2mn','prec',]

## load data 
cdata={}
for cvar in cvars:
    print("Loading:",cvar)
    cdata[cvar]=np.zeros((len(dlist)+ndayLead-1,nmembers))*np.nan
    for ik,ddate in enumerate(dlist):
        ## control forecasts 
        finput = os.path.join(BFDATA,f"AO-dd-cf-{cvar}-{ddate.strftime('%Y%m%d00')}.nc")
        if not os.path.isfile(finput):
            continue
        ds = xr.open_dataset(finput)
        ## extract singe lat/lon point
        #xtmp=ds[vnames[cvar]].sel(latitude=plat,longitude=plon,method='nearest')
        ## full domain mean 
        xtmp=ds[vnames[cvar]].mean(axis=(1,2))
        if ddate ==fcdate:
            cdata[cvar][ik:,0]=xtmp.values
        else:
            cdata[cvar][ik,0]=xtmp.isel(time=0)
        ds.close()  
          
        ## perturbed forecast
        finput = os.path.join(BFDATA,f"AO-dd-pf-{cvar}-{ddate.strftime('%Y%m%d00')}.nc")
        if not os.path.isfile(finput):
            continue
        ds = xr.open_dataset(finput)
        ## extract singe lat/lon point
        #xtmp=ds[vnames[cvar]].sel(latitude=plat,longitude=plon,method='nearest')
        ## full domain mean 
        xtmp=ds[vnames[cvar]].mean(axis=(2,3))
        if ddate ==fcdate:
            cdata[cvar][ik:,1:]=xtmp.values
        else:
            cdata[cvar][ik,1:]=xtmp.isel(time=0)
        ds.close()  
    if cvar in ['t2mn','t2mx']:
        cdata[cvar] =cdata[cvar] -273.16 # to degrees
    elif cvar in ['prec']:
        cdata[cvar] = cdata[cvar]*1000 # mm/day 




## Now make the plot 
mplt.rc('xtick', labelsize=8) 
mplt.rc('ytick', labelsize=8)


fig,axx = plt.subplots(3,1,figsize=(6,9),sharex=True)


for ik,cvar in enumerate(cvars):
    ax=axx[ik]

    ax.fill_between(dtime,
                    np.percentile(cdata[cvar],5,axis=1),
                    np.percentile(cdata[cvar],95,axis=1),
                    ec='b',fc='b',alpha=0.2)
    ax.plot(dtime,cdata[cvar].mean(axis=1),'b')
    ylim=ax.get_ylim()
    ax.plot([fcdate,fcdate],ylim,'k')
    ax.set_ylabel(cvar)    
         
plt.show()



