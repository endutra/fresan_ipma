# Script to get ECMWF soil moisture forecasts, compute soil moisture index and save to netcdf 
# E. Dutra April 2024 

# Set-up ecmwfapi 
#
# https://confluence.ecmwf.int/display/WEBAPI/Access+MARS
#
# pip install ecmwf-api-client
# or
# conda install -c conda-forge ecmwf-api-client
# 
# go to https://api.ecmwf.int/v1/key/
# copy to $HOME/.ecmwfapirc  (in windows C:\Users\Username\.ecmwfapirc ) 

# run as python extract_smi.py YYYYMMDD CREG
# YYYYMMDD - forecast initial date 
# CREG : region : currenlty only Ib is defined ! 

from ecmwfapi import api as eapi
import logging 
import xarray as xr 
import numpy as np
import datetime
import os 
import sys 


logging.basicConfig(
format='%(asctime)s extract_smi %(levelname)s: %(message)s',
        datefmt='%Y-%m-%d %H:%M:%S')
logger = logging.getLogger(__name__)

def mars_extract(req,fout,ntries=3,wait=5):
    
    ntry=1
    success = -1
    while ( ntry <= ntries ):
        try:
            server = eapi.ECMWFService("mars")
            server.execute(req,fout)
            success = 0
            break
        except:
            logger.error(f"Some error in mars extract try {ntry}")
            traceback.print_exc()
            ntry = ntry + 1 
            success = -1 
            time.sleep(wait)
            
    if success == -1:
        logger.error(f"Giving up of mars extraction")
        #raise Exception(f"Failed to get mars retrieve")
    
    return success


## Array with soil moisture definitions for each soil texture : Table 8.9 from IFS documentation
smt=np.array([ [ 0,0,0,0],  # zero  :   saturation, field capacity, wilting point, residual 
               [0.403,0.244,0.059,0.025], #coarse
               [0.439,0.347,0.151,0.010], # medium
               [0.430,0.383,0.133,0.010], # medium-fine
               [0.520,0.448,0.279,0.010], # fine 
               [0.614,0.541,0.335,0.010], # very fine 
               [0.766,0.663,0.267,0.010], # ext.trop organic
               [0.439,0.347,0.151,0.010] ])  # trop. organic

grid=".05/.05" # default output grid 

areas={'Ib':"45/-10/35/4"} # dictionary with areas definitions North/West/South/East
FCTIME='00'  # default forecast time 


# get command line arguments 
FCDATE = sys.argv[1] #"20240416"
CAREA  = sys.argv[2] #'Ib'

area=areas[CAREA]

# fancy logs ... 
logger.setLevel('DEBUG')
logger.info(f'Extract SMI start for {CAREA} {FCDATE} {FCTIME} ')


# extract soil type 
req={
    "class": "od",
    "date": FCDATE,
    "time": FCTIME,
    "expver": "1",
    "levtype": "sfc",
    "param": "slt",
    "stream": "oper",
    "grid": grid,
    "area": area,
    "type": 'an',
    "interpolation":"nearest neighbour",
    "format":"netcdf"
    }
fout_slt=f"slt_{CAREA}_{FCDATE}{FCTIME}.nc"
out_mars = mars_extract(req,fout_slt,ntries=3,wait=60)

# extract soil moisture forecasts 
req={
    "class": "od",
    "date": FCDATE,
    "time": FCTIME,
    "expver": "1",
    "levtype": "sfc",
    "param": "swvl1/swvl2/swvl3/swvl4",
    "stream": "oper",
    "grid": grid,
    "area": area,
    "type": 'fc',
    "step":'0/to/240/by/24',
    "interpolation":"nearest neighbour",
    "format":"netcdf"
    }
fout_sm=f"sm_{CAREA}_{FCDATE}{FCTIME}.nc"
out_mars = mars_extract(req,fout_sm,ntries=3,wait=60)

## load slt 
ds_static = xr.open_dataset(fout_slt)
slt = np.rint(ds_static.slt.values.squeeze()).astype('i2')
land = slt > 0 
ds_static.close()

## open sm data
ds_sm = xr.open_dataset(fout_sm)

## loop on each timestep 
for iks in range(ds_sm.time.size):

    ds = ds_sm.isel(time=[iks])
    #compute top meter soil moisture
    ds['sm'] = np.maximum(0,ds['swvl1']*0.07 + ds['swvl2']*0.21  + ds['swvl3']*0.72)
    
    # compute smi
    ds['smi'] = ds['sm']*0
    sm = ds['sm'].values.squeeze()
    sm[~land]=np.nan
    smi = sm * np.nan
    smi[land] = np.minimum(1,np.maximum(0, (sm[land]-smt[slt[land],2])/(smt[slt[land],1]-smt[slt[land],2]) ))
    ds['smi'].values[0,:] = smi

    ds['sm'].values[0,:] = sm*1000 # convert sm to mm 

    #save to netcdf output 
    dout = ds[['sm','smi']]

    validity_date = ds.time.values[0].astype('datetime64[us]').astype(datetime.datetime).strftime("%Y%m%d")
    fout = f"smi_{CAREA}_{FCDATE}{FCTIME}_{validity_date}{FCTIME}.nc"
    logger.info(f'Saving {fout}')
    dout.to_netcdf(fout,encoding={'sm':{"zlib": True, "complevel": 1},
                                  'smi':{"zlib": True, "complevel": 1}})

# clean
os.remove(fout_slt)
os.remove(fout_sm)
    
logger.info(f'Extract SMI finished {CAREA} {FCDATE} {FCTIME} ')   
